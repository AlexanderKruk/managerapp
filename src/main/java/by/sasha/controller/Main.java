package by.sasha.controller;

import by.sasha.assembly.Cars;
import by.sasha.assembly.Color;
import by.sasha.assembly.Engine;
import by.sasha.assembly.Transmission;
import by.sasha.cars.Toyota;
import by.sasha.utils.CarCollectionFiller;
import by.sasha.utils.CarFinder;
import by.sasha.utils.PriceSorter;
import by.sasha.utils.UserChoice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static by.sasha.controller.DIalogLogics.*;

public class Main {
    static Logger LOGGER= LogManager.getLogger("name");

    public static void main(String[] args) {
        Map<String, Toyota> carVariants = CarCollectionFiller.fillWithDifferentCars(new HashMap<String, Toyota>());
        boolean isCommandChosen = false;
        boolean isActive = true;
        boolean isSortOrderRecieved = false;
        boolean isModelRecieved = false;
        boolean isEngineRecieved = false;
        boolean isTransmissionRecieved = false;
        boolean isColorRecieved = false;
        boolean isPriceFloorChosen = false;
        boolean isPriceCeilingChosen = false;
        UserChoice userChoice = new UserChoice();
        int car = 1;
        while (isActive) {
            if (!isCommandChosen) {
                LOGGER.info("Выберите коммманду: \n 1.Рассчитать Цену \n 2.Сортировка комплектаций по цене\n" +
                        "3. Показать комплектации в диапазоне цен");
                Scanner sc = new Scanner(System.in);
                userChoice.setCommand(sc.nextInt());
            }
            switch (userChoice.getCommand()) {
                case 1:
                    isCommandChosen = true;
                    if (!isModelRecieved) {
                        Map<Integer, Cars> mapOrderedNumberWithCarName = assignNumberForEachCar();
                        LOGGER.info("Выберите машину: \n" +
                                loadAllAvailableCars());
                        Scanner sc = new Scanner(System.in);
                        int userInputCarNumber = sc.nextInt();
                        if (userInputCarNumber <= 3 && userInputCarNumber >= 0) {
                            Cars carName = findCarByUserInputAsKey(userInputCarNumber, mapOrderedNumberWithCarName);
                            userChoice.setCarModel(carName);
                            isModelRecieved = true;
                        } else {
                            LOGGER.info("Введите число соответствущее модели машины");
                            break;
                        }
                    }
                    if (!isEngineRecieved) {
                        Map<Integer, Engine> mapSequenceNumberWithEngineName = assignNumberForEachEngine();
                        LOGGER.info("Выберите объем двигателя \n" + loadAllAvailableEngines()
                        );
                        Scanner sc = new Scanner(System.in);
                        int userInputEngineNumber = sc.nextInt();
                        if (userInputEngineNumber <= 3 && userInputEngineNumber >= 0) {
                            Engine engineName = findEngineByUserInputAsKey(userInputEngineNumber, mapSequenceNumberWithEngineName);
                            userChoice.setEngine(engineName);
                            isEngineRecieved = true;
                        } else {
                            LOGGER.info("Введите число соответствущее объему двигателя");
                            break;
                        }
                    }
                    if (!isTransmissionRecieved) {
                        Map<Integer, Transmission> mapSequenceNumberWithGearbox = assignNumberForEachTransmission();
                        LOGGER.info("Выберите тип коробки передач \n" + loadAllAvailableTransmissions()
                        );
                        Scanner sc = new Scanner(System.in);
                        int userInputTransmissionNumber = sc.nextInt();
                        if (userInputTransmissionNumber <= 3 && userInputTransmissionNumber >= 0) {
                            Transmission transmissionName = findTransmissionByUserInputAsKey(userInputTransmissionNumber, mapSequenceNumberWithGearbox);
                            userChoice.setTransmission(transmissionName);
                            isTransmissionRecieved = true;
                        } else {
                            LOGGER.info("Введите число соответствущее типу коробки передач");
                            break;
                        }
                    }
                    if (!isColorRecieved) {
                        Map<Integer, Color> mapSequenceNumberWithColor = assignNumberForEachColor();
                        LOGGER.info("Выберите цвет \n" + loadAllAvailableColor()
                        );
                        Scanner sc = new Scanner(System.in);
                        int userInputColorNumber = sc.nextInt();
                        if (userInputColorNumber <= 8 && userInputColorNumber > 0) {
                            Color colorName = findColorByUserInputAsKey(userInputColorNumber, mapSequenceNumberWithColor);
                            userChoice.setColor(colorName);
                            isColorRecieved = true;
                        } else {
                            LOGGER.info("Введите число соответствущее цвету кузова");
                            break;
                        }
                    }
                    if (isColorRecieved) {
                        Toyota toyota = new Toyota(userChoice.getEngine(), userChoice.getColor(), userChoice.getTransmission(), userChoice.getCarModel());
                        LOGGER.info(String.format("Комплектация:%n%s%n", toyota.toString()));
                        isActive = false;
                    }
                    break;

                case 2:
                    isCommandChosen = true;
                    if (!isSortOrderRecieved) {
                        LOGGER.info("Выберите порядок сортировки: \n" +
                                "1. Убывающий\n 2. Возрастающий");
                        Scanner sc = new Scanner(System.in);
                        int userInputSortOrder = sc.nextInt();
                        if (userInputSortOrder == 1) {
                            Map<String, Toyota> sortedMap = PriceSorter.sortByPrice(carVariants, false);
                            CarFinder.findByPriceRange(sortedMap, 100000, 0);
                            isSortOrderRecieved = true;
                            isActive = false;
                        } else if (userInputSortOrder == 2) {
                            Map<String, Toyota> sortedMap = PriceSorter.sortByPrice(carVariants, true);
                            CarFinder.findByPriceRange(sortedMap, 100000, 0);
                            isSortOrderRecieved = true;
                            isActive = false;
                        } else {
                            isSortOrderRecieved = false;
                        }
                    }
                    break;

                case 3:
                    isCommandChosen = true;
                    if (!isPriceFloorChosen) {
                        LOGGER.info("Введите нижнюю границу цены");
                        Scanner sc = new Scanner(System.in);
                        double userInputPriceFloor = sc.nextDouble();
                        if (userInputPriceFloor <= 100000 && userInputPriceFloor > 0) {
                            userChoice.setPriceFloor(userInputPriceFloor);
                            isPriceFloorChosen = true;
                        } else {
                            isPriceCeilingChosen = false;
                            LOGGER.info("Нижняя граница цены может быть от 0 до 100 000 ");
                            break;
                        }
                    }
                    if (!isPriceCeilingChosen) {
                        LOGGER.info("Введите верхнюю границу цены");
                        Scanner sc = new Scanner(System.in);
                        double userIputPriceCeiling = sc.nextDouble();
                        if (userIputPriceCeiling <= 100000 && userIputPriceCeiling > 0 && userChoice.getPriceFloor() <= userIputPriceCeiling) {
                            userChoice.setPriceCeiling(userIputPriceCeiling);
                            isPriceCeilingChosen = true;
                        } else {
                            LOGGER.info("Верхняя граница цены может быть от 0 до 100 000 ");
                            break;
                        }
                    }
                    if (isPriceFloorChosen && isPriceCeilingChosen) {
                        CarFinder.findByPriceRange(carVariants, userChoice.getPriceCeiling(), userChoice.getPriceFloor());
                        isActive = false;
                        break;

                    }
                    default:
            }

        }
    }

}

