package by.sasha.controller;

import by.sasha.assembly.Cars;
import by.sasha.assembly.Color;
import by.sasha.assembly.Engine;
import by.sasha.assembly.Transmission;

import java.util.HashMap;
import java.util.Map;

public class DIalogLogics {


    protected static Color findColorByUserInputAsKey(int userInputCarNumber, Map<
            Integer, Color> mapOrderedNumberWithColorName) {
        return mapOrderedNumberWithColorName.get(userInputCarNumber);
    }

    protected static Cars findCarByUserInputAsKey(int userInputCarNumber, Map<
            Integer, Cars> mapOrderedNumberWithCarName) {
        return mapOrderedNumberWithCarName.get(userInputCarNumber);
    }

    protected static Transmission findTransmissionByUserInputAsKey(int userInputTransmissionNumber, Map<
            Integer, Transmission> mapOrderedNumberWithTransmissionName) {
        return mapOrderedNumberWithTransmissionName.get(userInputTransmissionNumber);
    }

    protected static Engine findEngineByUserInputAsKey(int userInputEngineNumber, Map<
            Integer, Engine> mapOrderedNumberWithTransmissionName) {
        return mapOrderedNumberWithTransmissionName.get(userInputEngineNumber);
    }

    protected static Map<Integer, Cars> assignNumberForEachCar() {
        Cars[] possibleValues = Cars.class.getEnumConstants();
        Map<Integer, Cars> carWithSequenceNumber = new HashMap<>();
        for (int i = 0; i < possibleValues.length; i++) {
            carWithSequenceNumber.put(i + 1, possibleValues[i]);
        }
        return carWithSequenceNumber;
    }

    protected static Map<Integer, Engine> assignNumberForEachEngine() {
        Engine[] possibleValues = Engine.class.getEnumConstants();
        Map<Integer, Engine> engineWithSequenceNumber = new HashMap<>();
        for (int i = 0; i < possibleValues.length; i++) {
            engineWithSequenceNumber.put(i + 1, (possibleValues[i]));
        }
        return engineWithSequenceNumber;
    }

    protected static Map<Integer, Transmission> assignNumberForEachTransmission() {
        Transmission[] possibleValues = Transmission.class.getEnumConstants();
        Map<Integer, Transmission> transmissionWithSequenceNumber = new HashMap<>();
        for (int i = 0; i < possibleValues.length; i++) {
            transmissionWithSequenceNumber.put(i + 1, (possibleValues[i]));
        }
        return transmissionWithSequenceNumber;
    }

    protected static Map<Integer, Color> assignNumberForEachColor() {
        Color[] possibleValues = Color.class.getEnumConstants();
        Map<Integer, Color> colorWithSequenceNumber = new HashMap<>();
        for (int i = 0; i < possibleValues.length; i++) {
            colorWithSequenceNumber.put(i + 1, possibleValues[i]);
        }
        return colorWithSequenceNumber;
    }

    protected static String loadAllAvailableEngines() {
        Engine[] possibleValues = Engine.class.getEnumConstants();
        StringBuilder outputEngines = new StringBuilder();
        for (int i = 0; i < possibleValues.length; i++) {

            outputEngines.append(i + 1).append(". ");
            outputEngines.append(possibleValues[i].getName());
            outputEngines.append("\n");
        }
        return outputEngines.toString();
    }

    protected static String loadAllAvailableTransmissions() {
        Transmission[] possibleValues = Transmission.class.getEnumConstants();
        StringBuilder outputTransmissions = new StringBuilder();
        for (int i = 0; i < possibleValues.length; i++) {
            outputTransmissions.append(i + 1).append(". ");
            outputTransmissions.append(possibleValues[i].getName());
            outputTransmissions.append("\n");
        }
        return outputTransmissions.toString();
    }

    protected static String loadAllAvailableColor() {
        Color[] possibleValues = Color.class.getEnumConstants();
        StringBuilder outputColors = new StringBuilder();
        for (int i = 0; i < possibleValues.length; i++) {
            outputColors.append(i + 1).append(". ");
            outputColors.append(possibleValues[i].getName());
            outputColors.append("\n");
        }
        return outputColors.toString();
    }

    protected static String formatOutput(Object[] possibleValues) {
        StringBuilder outPutEngines = new StringBuilder();
        for (Object eng : possibleValues) {
            outPutEngines.append(eng.toString());
            outPutEngines.append("\n");
        }
        return outPutEngines.toString();
    }

    protected static String loadAllAvailableCars() {
        Cars[] possibleValues = Cars.class.getEnumConstants();
        StringBuilder outPutCars = new StringBuilder();
        for (int i = 0; i < possibleValues.length; i++) {

            outPutCars.append(i + 1).append(". ");
            outPutCars.append(possibleValues[i].getName());
            outPutCars.append("\n");
        }
        return outPutCars.toString();
    }

}
