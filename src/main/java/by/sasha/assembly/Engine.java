package by.sasha.assembly;

public enum Engine {
    CAPACITY_1_8("1.8",2700),
    CAPACITY_2_0("2.0",3000),
    CAPACITY_3_0("3.0",3800);

    private double price;
    private String name;

    Engine(String name, double price) {
        this.price = price;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void String(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
