package by.sasha.assembly;

public enum Transmission {
    AUTO("Auto",1500),
    MANUAL("Manual",700),
    CVT("CVT",1900);
    private double price;
    private String name;

    Transmission(String name,double price) {
        this.price = price;
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void String(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
