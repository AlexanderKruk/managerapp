package by.sasha.assembly;

public enum Color {
    BLACK("Black",210),
    WHITE("White",200),
    RED("Red",350),
    BLUE("Blue",270),
    GREY("Grey",270),
    GREEN("Green",100),
    BROWN("Brown",100),
    ORANGE("Orange",390);
    private double price;
private String name;

    Color(String name,double price) {
        this.price = price;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
