package by.sasha.assembly;

public enum Cars {
    CORROLA("Corrola",8000),
    CAMRY("Camry",12000),
    LAND_CRUISER("Land Cruiser",45000);

    private String name;
    private double price;

    Cars(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void getName(double price) {
        this.name = name;
    }

}
