package by.sasha.cars;

import by.sasha.assembly.Cars;
import by.sasha.assembly.Color;
import by.sasha.assembly.Engine;
import by.sasha.assembly.Transmission;
import com.sun.javafx.binding.StringFormatter;

public class Toyota {
    private Cars model;
    private Engine engineCapacity;
    private Color color;
    private Transmission transmission;
    private double totalPrice;

    public Toyota(Engine engineCapacity, Color color, Transmission transmission, Cars model) {
        this.engineCapacity = engineCapacity;
        this.color = color;
        this.transmission = transmission;
        this.model = model;
        this.setTotalPrice();
    }

    public Toyota() {
    }

    public Cars getModel() {
        return model;
    }

    public void setModel(Cars model) {
        this.model = model;
        this.setTotalPrice();
    }

    public Engine getEngineCapacity() {
        return engineCapacity;
    }

    public Color getColor() {
        return color;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice() {
        this.totalPrice = this.model.getPrice() + this.engineCapacity.getPrice() + this.color.getPrice() + this.transmission.getPrice();
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setEngineCapacity(Engine engineCapacity) {
        this.engineCapacity = engineCapacity;
        this.setTotalPrice();
    }

    public void setColor(Color color) {
        this.color = color;
        this.setTotalPrice();
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
        this.setTotalPrice();
    }

    public final String generateName() {

        return String.valueOf(StringFormatter.format("%s %s %s %s", this.getModel().getName(),
                this.getEngineCapacity().getName(), this.getTransmission().getName(), this.getColor().getName()));

    }

    @Override
    public String toString() {
        return "Toyota{" +
                "Название модели :" + model.getName() +
                ", Объем двигателя :" + engineCapacity.getName() +
                ", Цвет :" + color.getName() +
                ", Тип коробки передач :" + transmission.getName() +
                ", Полная цена :" + totalPrice +
                '}';
    }
}
