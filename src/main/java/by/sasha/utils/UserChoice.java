package by.sasha.utils;

import by.sasha.assembly.Cars;
import by.sasha.assembly.Color;
import by.sasha.assembly.Engine;
import by.sasha.assembly.Transmission;

public class UserChoice {
    private int command;
    private Cars carModel;
    private Engine engine;
    private Transmission transmission;
    private Color color;
    private double priceCeiling;
    private double priceFloor;

    public UserChoice() {
    }

    public Cars getCarModel() {
        return carModel;
    }

    public void setCarModel(Cars carModel) {
        this.carModel = carModel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public Color getColor() {
        return color;
    }

    public double getPriceCeiling() {
        return priceCeiling;
    }

    public void setPriceCeiling(double priceCeiling) {
        this.priceCeiling = priceCeiling;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public double getPriceFloor() {
        return priceFloor;
    }

    public void setPriceFloor(double priceFloor) {
        this.priceFloor = priceFloor;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "UserChoice{" +
                "command=" + command +
                ", carModel=" + carModel +
                ", engine=" + engine +
                ", transmission=" + transmission +
                ", color=" + color +
                ", priceCeiling=" + priceCeiling +
                ", priceFloor=" + priceFloor +
                '}';
    }
}
