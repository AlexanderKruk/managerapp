package by.sasha.utils;

import by.sasha.assembly.Cars;
import by.sasha.assembly.Color;
import by.sasha.assembly.Engine;
import by.sasha.assembly.Transmission;
import by.sasha.cars.Toyota;

import java.util.Map;

public class CarCollectionFiller {

    private CarCollectionFiller() {
    }

    public static Map<String, Toyota> fillWithDifferentCars(Map<String, Toyota> carsMap) {
        Toyota toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_1_8, Color.BLUE, Transmission.MANUAL, Cars.LAND_CRUISER);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_3_0, Color.GREEN, Transmission.AUTO, Cars.LAND_CRUISER);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.GREEN, Transmission.CVT, Cars.LAND_CRUISER);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_1_8, Color.GREEN, Transmission.AUTO, Cars.CAMRY);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.BLACK, Transmission.CVT, Cars.CAMRY);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_1_8, Color.BLACK, Transmission.CVT, Cars.CORROLA);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.RED, Transmission.CVT, Cars.CORROLA);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.RED, Transmission.MANUAL, Cars.CORROLA);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.BLUE, Transmission.AUTO, Cars.CORROLA);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        toyotaToInsertInVariantsDatabase = new Toyota(Engine.CAPACITY_2_0, Color.BLACK, Transmission.CVT, Cars.CORROLA);
        carsMap.put(toyotaToInsertInVariantsDatabase.generateName(), toyotaToInsertInVariantsDatabase);
        return carsMap;
    }
}
