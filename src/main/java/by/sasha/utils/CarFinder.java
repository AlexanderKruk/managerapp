package by.sasha.utils;

import by.sasha.cars.Toyota;

import java.util.Map;

public class CarFinder {

    private CarFinder() {
    }

    public static void findByPriceRange(Map<String, Toyota> cars, double maxPrice, double minPrice) {
        //@TODO LOGGER
        for (Map.Entry<String, Toyota> toyota : cars.entrySet()) {
            Toyota currentToyota = (toyota.getValue());
            if ((currentToyota.getTotalPrice()) > minPrice &&
                    (currentToyota.getTotalPrice()) < maxPrice) {
                Printer.display(currentToyota);
            }

        }
    }

}

