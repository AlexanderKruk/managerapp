package by.sasha.utils;

import by.sasha.cars.Toyota;

import java.util.*;

public class PriceSorter {

    public static Map<String, Toyota> sortByPrice(Map<String, Toyota> cars, final boolean order) {

        List<Map.Entry<String, Toyota>> carsToSort = new LinkedList<>(cars.entrySet());

        // Sorting the list based on values
        carsToSort.sort((car1, car2) -> {
            if (order) {
                return sort(car1, car2);
            } else {
                return sort(car2, car1);
            }
        });

        // Maintaining insertion order with the help of LinkedList
        LinkedHashMap<String, Toyota> sortedCars = new LinkedHashMap<>();
        for (Map.Entry<String, Toyota> entry : carsToSort) {
            sortedCars.put(entry.getKey(), entry.getValue());
        }

        return sortedCars;
    }

    private static int sort(Map.Entry<String, Toyota> car1, Map.Entry<String, Toyota> car2) {
        if (car1.getValue().getTotalPrice() < car2.getValue().getTotalPrice())
            return -1;
        else if (car2.getValue().getTotalPrice() < car1.getValue().getTotalPrice())
            return 1;
        return 0;
    }


}
