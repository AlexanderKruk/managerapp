package by.sasha.utils;

import by.sasha.cars.Toyota;

public class PriceCalculator {

    private PriceCalculator() {
    }

    //@TODO UNITE ALL METHODS IN ONE CLASS
    public static String getCarTotalPrice(Toyota toyota) {
        return String.valueOf(toyota.getTotalPrice());
    }

}
