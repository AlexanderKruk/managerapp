package by.sasha.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ArgumentsChecker {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArgumentsChecker.class);

    private ArgumentsChecker() {
    }
    public static boolean checkArgs(String[] args) {
        boolean resultFlag = true;
        if (args.length != 0) {
            switch (args[0]) {
                case "sort": {
                    if (args.length != 2) {
                        resultFlag = false;
                        break;
                    }
                    if (!(args[1].equalsIgnoreCase("ascending")) &&
                            !(args[1].equalsIgnoreCase("descending"))) {
                        LOGGER.info("Sort order is not set properly");
                        resultFlag = false;
                    }

                    break;
                }
                case "find": {
                    if (args.length != 3) {
                        resultFlag = false;
                    } else if (Double.parseDouble(args[1]) < 0 || Double.parseDouble(args[1]) > 100000 ||
                            Double.parseDouble(args[2]) < 0 || Double.parseDouble(args[2]) > 100000 ||
                            (Double.parseDouble(args[1]) > Double.parseDouble(args[2]))) {
                        LOGGER.info("Price Range is false");
                        resultFlag = false;
                    }
                    break;
                }
                case "calculate":
                    if (args.length != 2) {
                        resultFlag = false;
                    }
                    break;
                default: {
                    LOGGER.info("First argument is not valid");
                    resultFlag = false;
                }
            }
        } else {
            resultFlag = false;
        }
        return resultFlag;
    }
}
