package by.sasha.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Printer {
    static Logger LOGGER= LogManager.getLogger("name");

    private Printer() {
    }

    public static void display(Object object) {
        LOGGER.info("{}", object);
    }
}
